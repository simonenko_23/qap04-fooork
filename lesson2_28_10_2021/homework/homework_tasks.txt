!!!IMPORTANT!!!
All homework from now should be pushed to the bitbucket repository: https://bitbucket.org/al_ast/qap04 repository
The rule is relevant for all others homework

Branches name: feature/lesson2/firstname_lastname
Tasks 2-4 should be placed under lesson2_28_10_2021/homework/folder_with_your_firstname_lastname see lesson1 example
python files names: task_1, task_2 e.t.c

E.g lesson 3 will have:
branch_name: feature/lesson3/alex_astapov(your name here)
folder structure: qap04/lesson3_01_11_2021/homework/alex_astapov/files with homework
files names: task_1.py, task_2.py, task_3.pu

!!!Tasks with incorrect branches names or folder structure won't be accepted.
!!!Keep the rule for the next tasks and lessons.

Read PEP8:
Имена модулей и пакетов, имена классов, имена функций/методов, имена методов и переменных экземпляров класса
Try to follow the namings rules. For the next few lessons it will be acceptable to use incorrect names however
I will start to treat it as a part of homework
________________________________________________________________________________________________________________________
Deadline: 04.11.2021

Task1:
1. FORK repository https://bitbucket.org/al_ast/qap04
2. Set git config with your user name and email
3. Create a new branch
4. Make any changes
5. Push them to the bitbucket with any sensitive message
6. Create a pull request
https://www.atlassian.com/git/tutorials/making-a-pull-request
6. Merge it
7.Send me the link on the result

Task2
Apply all math operations e.g +,-, **, % e.t.c to the variable itself
e.g 
x=5
x+=3
print(x)

Task3
print all comparison operations result
e.g
print(5 > 9)
print(2 != 3)
e.t.c

Task3*
Read user input from the console
For negative number print: “exact number is negative”
For positive number print whether it is odd or even: e.g “exact number is odd”
For strings print: “Only numbers are supported”

Task4**
Create a car with color, model, current mileage, max mileage. By default max mileage=current mileage + 1000
Car should have 2 methods:
1.print_info: which will print all information about car e.g “This is color model car”
2. drive. Which accepts kms to drive. Method should print any message every km it has drive.
If it is more or equals max mileage then car is broken and should print it

e.g
current mileage=8
max mileage=12
Result of the method with 2 kms
My current mileage is 9
My current mileage is 10
Result of the method with 3 kms
My current mileage is 11
I’m broken because of my current mileage is maximum(12).






