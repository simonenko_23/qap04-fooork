x = 4
x += 8
print(x)

y = 9
y -= 5
print(y)

z = 7
z *= 6
print(z)

d = 22
d /= 11
print(d)

a = 31
a //= 2
print(a)

b = 23
b %= 4
print(b)

c = 8
c **= 2
print(c)