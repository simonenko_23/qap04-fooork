a = 5

# while a < 10:
#     print(a)
#     a += 1

# a = 5
# while a < 10:
#     print(f"Current {a=}")
#
#     if a == 8:
#         print(f"Leave(break) while with {a=}")
#         break
#
#     print(f"Code after break. {a=}")
#
#     a += 1
#
#     if a == 7:
#         print(f"Continue while. Next iteration {a=}")
#         continue
#
numbers_range = range(10)
#
# for number in numbers_range:
#     print(f"For {number=}")


for number in numbers_range:
    for number2 in numbers_range:
        print(f"For {number=}. {number2=}")
        if number2 == 4:
            break

for number in numbers_range:
    for number2 in numbers_range:
        print(f"For {number=}. {number2=}")
        if number2 == 4:
            continue
